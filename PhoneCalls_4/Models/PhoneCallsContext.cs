using Microsoft.EntityFrameworkCore;
using PhoneCalls_4.Models;
using Microsoft.Extensions.Configuration;

namespace PhoneCalls_4.Models
{
    public class PhoneCallsContext : DbContext
    {
        public DbSet<Calls> Calls { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Persons> Persons { get; set; }

        public PhoneCallsContext(DbContextOptions<PhoneCallsContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
    }
}