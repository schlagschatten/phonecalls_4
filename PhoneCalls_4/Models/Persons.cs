using System.Collections.Generic;

namespace PhoneCalls_4.Models
{
    public class Persons
    {
        public int id { get; set; }
        public string first_name { get; set; }
        public string second_name { get; set; }
        public string last_name { get; set; }
        public string phone_number { get; set; }
        public Address address { get; set; }
        public List<Calls> calls { get; set; }
    }
}