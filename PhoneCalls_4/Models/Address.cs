using System.Collections.Generic;

namespace PhoneCalls_4.Models
{
    public class Address
    {
        public int id { get; set; }
        public string city { get; set; }
        public string street { get; set; }
        public string house_number { get; set; }
        public List<Persons> persons { get; set; }
    }
}